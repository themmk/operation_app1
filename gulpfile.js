'use strict';
var gulp = require("gulp");
var sass = require("gulp-sass");
var imagemin = require("gulp-imagemin");
var cleanCSS = require("gulp-clean-css");
gulp.task("newCss", function () {
  return gulp.src("./sass/main.scss")
    .pipe(sass())
    .pipe(gulp.dest("./css"));

});

gulp.task("minify-css", function () {
    return gulp.src("'./css/*.css'")
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./js'));
  });


gulp.task("izle", function(){
	gulp.watch('./sass/*.scss', ['newCss'])
});

gulp.task('images', function () {
    return gulp.src('./img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./img'));
  });
// Static Server + watching scss/html files


// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("./sass/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("./css"));
      });

gulp.task('default', ['izle', 'newCss',  'images', 'minify-css', 'sass'])
