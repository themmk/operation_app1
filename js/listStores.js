$(document).ready(function () {

    $('.denetim').click(function () {
        $('#selected').show();
        let optionVal = $('#selected option').attr("value");
        $('#selected').on('change', function (e) {
            const optionSelected = $("option:selected", this);
            const valueSelected = this.value;
            const idSelected =optionSelected.attr('id');
            console.log("id selected: "+optionSelected.selectedIndex);
            if (valueSelected != "") {
                //window.location.href = './audits.php';
                Swal({
                    title: 'SEÇİLEN MAĞAZA: ' + `${valueSelected}`,
                    type: 'success',
                    confirmButtonText: 'Yönlendiriliyorsunuz..',
                }),
                    setTimeout(function () {
                        window.location.href = "audits.php?store=" + `${idSelected}`;
                    }, 3000);
            }
        });
    });

})