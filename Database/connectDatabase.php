<?php

function connectDB($db_name)
{
    $conn = new PDO ("mysql:host=localhost;dbname=$db_name", "root", "");
    $conn->exec("SET NAMES 'utf8'; SET CHARSET 'utf8'");
    return $conn;
}

function getFormQuestions($dbName)
{

    $db = connectDB($dbName);
    $getQuestionForm = $db->prepare("SELECT * FROM `form`");
    $getQuestionForm->execute();
    $fQF = $getQuestionForm->fetchAll();
    if ($fQF != null) {
        foreach ($fQF as $row) {
            echo "location: " . $row["location"] . "  subject " . $row["subject"] . "  description " . $row["description"] . "<br>";
            echo "<br>";
        }
    } else {
        echo "getFormQuestions data çekilemedi";
        echo "<br>";
    }
}

//TODO: to get all questions and its answer of dedicated store and date
function getAnswersWithQuestions()
{
    $db = connectDB("safety_diag");
    $getQuestionForm = $db->prepare("SELECT `form`.`id`,`form`.`subject`,`form`.`description`,`813`.`diag_detail_id`,`813`.`status`,`813`.`comment`,`813`.`action_plan` FROM `813` JOIN `form` ON `813`.`form_id`= `form`.`id`AND `813`.`diag_detail_id`=2");
    $getQuestionForm->execute();
    $fQF = $getQuestionForm->fetchAll();
    $countYes = 0;
    $countNo = 0;
    $countNA = 0;
    if ($fQF != null) {

        foreach ($fQF as $row) {

            if ($row["status"] == "YES") {
                $countYes++;
            } ELSEIF ($row["status"] == "NO") {
                $countNo++;
            } ELSEIF ($row["status"] == "NA") {
                $countNA++;
            } else {

            }
            echo "form id: " . $row["id"] . "  subject " . $row["subject"] . "  comment " . $row["comment"] . "  status " . $row["status"] . "<br>";
            echo "<br>";
        }
    } else {

        echo "getfull data çekilemedi";
        echo "<br>";
    }

    echo "TOPLAM YES: " . $countYes . " NO:" . $countNo . " NA:" . $countNA;
    echo "<br>";
}

//TODO:

function TestArray()
{
    $db = connectDB("safety_diag");
    $getQuestionForm = $db->prepare("SELECT * FROM `form`");
    $getQuestionForm->execute();
    $fQF = $getQuestionForm->fetchAll();

    $status = array("yes", "no", "yes", "na");

    $listSubject = array();
    foreach ($fQF as $value) {

        if (!in_array($value["subject"], $listSubject)) {
            $key = $value["subject"];

            for ($i = 0; $i < 4; $i++) {
                //echo $i . " değeri : ". $status[$i];
                $listSubject[$key][$i] = $status[$i];
            }
        }

    }
    print_r($listSubject);
}

?>