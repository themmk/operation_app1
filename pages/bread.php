<?php
function breadcrumbs($home = 'Anasayfa') {
  global $page_title; 
    $breadcrumb  = '<nav aria-label="breadcrumb"><ol class="breadcrumb">';
    $root_domain = ($_SERVER['HTTPS'] ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'].'/';
    $breadcrumbs = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
    $breadcrumb .= '<li class="breadcrumb-item"><a href="' . $root_domain . '"><span>' . "..." . '</span></a></li>';
    foreach ($breadcrumbs as $crumb) {
        $link = ucwords(str_replace(array(".php","-","_"), array(""," "," "), $crumb));
        $root_domain .=  $crumb . '/';
        $breadcrumb .= '<li class="breadcrumb-item"><a href="'. $root_domain .'" title="'.$page_title.'"><span>' . $link . '</span></a></li>';
    }
    $breadcrumb .= '</ol>';
    $breadcrumb .= '</nav>';
    return $breadcrumb;
}
echo breadcrumbs();
?>