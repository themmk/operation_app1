<div class="safetyPage">

    <div class="safetypagetop">
        <div class="safetyPageInfo safetyWrap">
            <div>
                <span>mağaza kodu: <?php
                    echo $_GET['store']; ?></span>      <!--Mağaza kodu !-->
            </div>
            <div class="safetyInlineWrapper">
                <span>yes</span>
                <span>no</span>
                <span>n/a</span>
            </div>
        </div><!-- safetyPageInfo -->
        <div class="safetyPageInfo safetyWrap">

            <div class="basariIlerlemeBaslik">

                <?php
                include "../Database/connectDatabase.php";
                viewSubjectResultAll("safety_diag");
                function viewSubjectResultAll($dbName)
                {

                    $dba = connectDB($dbName);

                    $countYes = 0;
                    $countNo = 0;
                    $countNA = 0;
                    $countNDone = 0;

                    $getQuestionForm = $dba->prepare("SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` LEFT JOIN `form` ON `813`.`form_id`=`form`.`id` WHERE `813`.`diag_detail_id`=3 UNION SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` RIGHT JOIN `form` ON `813`.`form_id`=`form`.`id`WHERE `813`.`diag_detail_id`=3 ");
                    $getQuestionForm->execute();
                    $tableResult = $getQuestionForm->fetchAll();

                    foreach ($tableResult as $deneme) {
                        if ($deneme["status"] == 'YES') {
                            $countYes++;
                        } elseif ($deneme["status"] == 'NO') {
                            $countNo++;
                        } elseif ($deneme["status"] == 'NA') {
                            $countNA++;
                        } elseif ($deneme["status"] == '') {
                            $countNDone++;
                        }

                    }
                    $doneRate = calculateDoneRate($countYes, $countNo, $countNA, count($tableResult));

                    $succesRate = calculateSucRate($countYes, $countNo, $countNA, $countNDone, count($tableResult));

                    echo "<div>
                    <span>başarı</span>
                    <span>" . $succesRate . " %</span>   </div>";
                    echo "
                <div>
                    <span>ilerleme</span>
                    <span>" . $doneRate . "%</span>
                </div> </div>
            <div class=\"safetyInlineWrapper\">
                <span>" . $countYes . "</span>
                <span>" . $countNo . "</span>
                <span>" . $countNA . "</span>
            </div>";

                    $db = null;
                }

                function calculateSucRateAll($yesNum, $noNum, $naNum, $nDoneNum, $totalNum)
                {
                    $x = ($yesNum) / ($totalNum - ($naNum + $nDoneNum));
                    return $x * 100;
                }

                function calculateDoneRateAll($yesNum, $noNum, $naNum, $totalNum)
                {
                    $x = ($yesNum + $noNum + $naNum) / ($totalNum);
                    return $x * 100;
                }

                ?>


            </div>
        </div><!-- safetypagetop -->
        <div class="safetypagebottom">

            <?php

            viewSubjectResult("safety_diag");
            function viewSubjectResult($dbName)
            {
                $store = $_GET["store"];
                $dateID = $_GET["date"];
                $str = '';
                $db = connectDB($dbName);
                $getQuestionForm = $db->prepare("SELECT * FROM `form`");
                $getQuestionForm->execute();
                $fQF = $getQuestionForm->fetchAll();

                $listSubject = array();
                foreach ($fQF as $value) {
                    $key = $value["subject"];

                    if (!in_array($value["subject"], $listSubject)) {

                        $listSubject[$key][] = $value["id"];
                    } else {

                        $listSubject[$key][] = $value["id"];
                    }
                }

                foreach ($listSubject as $value => $a) {
                    echo $value;
                    echo "<script>
                $('.safetypagebottom').append('<a href=\"./comments.php?subject=$value&&store=$store&&date=$dateID\">\
    <div class=\"controller\">\
    <div class=\"controllerTitle\">$value</div><!--controllerTitle-->\
    ";

                    $countYes = 0;
                    $countNo = 0;
                    $countNA = 0;
                    $countNDone = 0;

                    $getQuestionForm = $db->prepare("SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` LEFT JOIN `form` ON `813`.`form_id`=`form`.`id` WHERE `813`.`diag_detail_id`=3 AND `form`.`subject`='$value' UNION SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` RIGHT JOIN `form` ON `813`.`form_id`=`form`.`id`WHERE `813`.`diag_detail_id`=3 AND `form`.`subject`='$value'");
                    $getQuestionForm->execute();
                    $tableResult = $getQuestionForm->fetchAll();

                    foreach ($tableResult as $deneme) {
                        if ($deneme["status"] == 'YES') {
                            $countYes++;
                        } elseif ($deneme["status"] == 'NO') {
                            $countNo++;
                        } elseif ($deneme["status"] == 'NA') {
                            $countNA++;
                        } elseif ($deneme["status"] == '') {
                            $countNDone++;
                        }
                        $doneRate = calculateDoneRate($countYes, $countNo, $countNA, count($tableResult));
                        $succesRate = calculateSucRate($countYes, $countNo, $countNA, $countNDone, count($tableResult));
                    }
                    echo "<div class=\"controllerContent\">\
            <div>\
                <span>Başarı</span>    \
                <span>' + $succesRate + '</span>    \
            </div>\
            <div>\
                <span>İlerleme</span>    \
                <span>' + $doneRate + '</span>    \
            </div>\
    </div><!--controllerContent-->\
    ";


                    echo "</div><!-- controller -->\
                    </a>');
</script>";

                }
            }

            function calculateSucRate($yesNum, $noNum, $naNum, $nDoneNum, $totalNum)
            {
                $x = ($yesNum) / ($totalNum - ($naNum + $nDoneNum));
                return $x * 100;
            }

            function calculateDoneRate($yesNum, $noNum, $naNum, $totalNum)
            {
                $x = ($yesNum + $noNum + $naNum) / ($totalNum);
                return $x * 100;
            }

            ?>
        </div><!-- safetypagebottom -->


    </div><!-- safetyPage -->

<?php

/* <!-- auditsButton -->
    <div class="safetybuttons">
        <a href="./comments.php" class="cta right yellow add_to_cart_button">sonraki adım</a>
    </div><!-- auditsButton -->
if (isset($_GET['date'])) {
    if (session_id() == '') {
        session_start();
    }

    echo "hello date:  " . $_GET['date'];
}
if (isset($_GET['diag'])) {
    if (session_id() == '') {
        session_start();
    }

    echo "dataa:  " . ($_GET['diag']);

    foreach ($_SESSION["files"] as $row) {
        echo $_SESSION["uid"];
        if (password_verify($row["db_name"], $_GET['diag'])) {

            $a = $row["db_name"];
            echo " datam : " . $a;
        }
    }
}
*/
?>