<?php
include "../Database/connectDatabase.php";

//TODO: DB adi, store number, diag_id gelmeli.
//  viewSubjectResult("safety_diag");
function viewSubjectResult($dbName)
{
    $str = '';
    $db = connectDB($dbName);
    $getQuestionForm = $db->prepare("SELECT * FROM `form`");
    $getQuestionForm->execute();
    $fQF = $getQuestionForm->fetchAll();

    $listSubject = array();
    foreach ($fQF as $value) {
        $key = $value["subject"];

        if (!in_array($value["subject"], $listSubject)) {

            $listSubject[$key][] = $value["id"];
        } else {

            $listSubject[$key][] = $value["id"];
        }
    }

    foreach ($listSubject as $value => $a) {
        echo "value :" . $value;
        echo "<br>";


        $countCritical = 0;
        $countImprove = 0;
        $countSatisfactory = 0;
        $countNAudited=0;
        $countNConcerned=0;
        $getQuestionForm = $db->prepare("SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` LEFT JOIN `form` ON `813`.`form_id`=`form`.`id` WHERE `813`.`diag_detail_id`=3 AND `form`.`subject`='$value' UNION SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` RIGHT JOIN `form` ON `813`.`form_id`=`form`.`id`WHERE `813`.`diag_detail_id`=3 AND `form`.`subject`='$value'");
        $getQuestionForm->execute();
        $tableResult = $getQuestionForm->fetchAll();

        foreach ($tableResult as $deneme) {
            if ($deneme["status"] == 'CRITICAL') {
                $countCritical++;
            } elseif ($deneme["status"] == 'IMPROVE') {
                $countImprove++;
            } elseif ($deneme["status"] == 'SATISFACTORY') {
                $countSatisfactory++;
            } elseif ($deneme["status"] == 'NOT AUDITED') {
                $countNAudited++;
            }
            elseif ($deneme["status"] == 'NOT CONCERNED') {
                $countNConcerned++;
            }
            $doneRate = calculateDoneRate($countCritical,$countImprove,$countSatisfactory,$countNAudited,$countNConcerned,count($tableResult));
            $succesRate = calculateSucRate($countSatisfactory, $countCritical,$countImprove, $countNAudited,$countNConcerned, count($tableResult));


        }
        echo "critical:".$countCritical."improve: ".$countImprove." satis: ". $countSatisfactory." Naudited: ".$countNAudited. "Nconcerned: ".$countNConcerned;
        echo "<br>";
        echo "success: " . $succesRate;
        echo "<br>";
        echo "<br>";

    }
}

function calculateSucRate($criticalNum, $improveNum, $satisNum,$nauditNum,$nconcernNum,$totalNum)
{
    $x = ($satisNum +$improveNum) / ($totalNum - ($criticalNum+$nauditNum+$nconcernNum));
    return $x * 100;
}

function calculateDoneRate($criticalNum, $improveNum, $satisNum,$nauditNum,$nconcernNum,$totalNum)
{
    echo $totalNum;
    $x = ($criticalNum + $improveNum +$satisNum +$nauditNum+$nconcernNum) / ($totalNum);
    return $x * 100;
}

?>