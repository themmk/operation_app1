<?php
include "../Database/connectDatabase.php";

//TODO: DB adi, store number, diag_id gelmeli.
//  viewSubjectResult("safety_diag");
function viewSubjectResult($dbName)
{
    $str = '';
    $db = connectDB($dbName);
    $getQuestionForm = $db->prepare("SELECT * FROM `form`");
    $getQuestionForm->execute();
    $fQF = $getQuestionForm->fetchAll();

    $listSubject = array();
    foreach ($fQF as $value) {
        $key = $value["subject"];

        if (!in_array($value["subject"], $listSubject)) {

            $listSubject[$key][] = $value["id"];
        } else {

            $listSubject[$key][] = $value["id"];
        }
    }

    foreach ($listSubject as $value => $a) {
        echo "value :" . $value;
        echo "<br>";


        $countYes = 0;
        $countNo = 0;
        $countNA = 0;
        $countNDone = 0;

        $getQuestionForm = $db->prepare("SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` LEFT JOIN `form` ON `813`.`form_id`=`form`.`id` WHERE `813`.`diag_detail_id`=3 AND `form`.`subject`='$value' UNION SELECT `813`.`status`,`813`.`form_id`,`form`.`subject` FROM `813` RIGHT JOIN `form` ON `813`.`form_id`=`form`.`id`WHERE `813`.`diag_detail_id`=3 AND `form`.`subject`='$value'");
        $getQuestionForm->execute();
        $tableResult = $getQuestionForm->fetchAll();

        foreach ($tableResult as $deneme) {
            if ($deneme["status"] == 'YES') {
                $countYes++;
            } elseif ($deneme["status"] == 'NO') {
                $countNo++;
            } elseif ($deneme["status"] == 'NA') {
                $countNA++;
            } elseif ($deneme["status"] == '') {
                $countNDone++;
            }
            $doneRate = calculateDoneRate($countYes, $countNo, $countNA, count($tableResult));
            $succesRate = calculateSucRate($countYes, $countNo, $countNA, $countNDone, count($tableResult));


        }
        echo "   yes:  " . $countYes . "  no:   " . $countNo . "  NA:    " . $countNA . " done   %" . $doneRate;
        echo "<br>";
        echo "success: " . $succesRate;
        echo "<br>";
        echo "<br>";

    }
}

function calculateSucRate($yesNum, $noNum, $naNum, $nDoneNum, $totalNum)
{
    $x = ($yesNum) / ($totalNum - ($naNum+$nDoneNum));
    return $x * 100;
}

function calculateDoneRate($yesNum, $noNum, $naNum, $totalNum)
{
    echo $totalNum;
    $x = ($yesNum + $noNum + $naNum) / ($totalNum);
    return $x * 100;
}

?>