<!-- Modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Audits veya Diagnostic Seçiminizi Yapın</h5>
      </div>
      <div class="modal-body" id="modalID">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
        <button type="button" class="btn btn-primary">Kaydet</button>
      </div>
    </div>
  </div>
</div> -->
<!-- 
  her sayfaya breadcrumb eklenicek.
  
  
  tarihlerden herhangi birisi seçildiği anda sonraki adım butonu aktifleştirilecek.
  
  
   -->
<!--
-->

<?php
include "../Database/connectDatabase.php";

//getFileAuthority();
if ($_GET["store"]) {
    //  echo "Welcome store: " . $_GET["store"] . "<br />";
}

//TODO: to get authorized user's accessible files where site is defined:
// kullanıcının tanımlı lokasyonda hangi dosyalara erisebileceginin alinir
function getFileAuthority()//($right, $storeNumber)
{
    session_start();
    $storeNumber = "";
    $right = $_SESSION["right"];

    if ($_SESSION["site"] != "all") {
        $storeNumber = $_SESSION["site"];
    } else {
        $storeNumber = $_GET["store"];
    }
    $_SESSION["files"] = Null;
    if ($_SESSION["files"] == Null) {
        //  echo "file:****: " . $storeNumber;
        try {
            $db = connectDB("users");
            /* echo " get file baglanti basarili";
             echo "<br>";*/
            $tableName = $right . "_files";  //kullanıcı yetkisine göre db'ye bağlanacak.
            $findFileName = $db->prepare("SELECT `file_name`,`db_name` FROM $tableName");
            $findFileName->execute();
            $fFN = $findFileName->fetchAll();
            $_SESSION["files"] = $fFN;
            //print_r($_SESSION["files"]);
            if ($fFN != null) {
                foreach ($_SESSION["files"] as $row) {
                    /* echo "<br>";
                     echo "file name: " . $row["file_name"] . "<br>";
                     echo "db name: " . $row["db_name"] . "<br>";*/
                    $fileName = $row["file_name"];
                    $dbName = $row["db_name"];
                    $fileDates = getFileDates($row["db_name"], $storeNumber);

                    echo "<div class=\"card-header\" id=\"headingThree\">
                        <h5 class=\"mb-0 text-center\">
                            <button class=\"btn btn-link collapsed\" data-toggle=\"collapse\" data-target=\"#collapseThree\"
                                    aria-expanded=\"false\" aria-controls=\"collapseThree\">
                                $fileName
                            </button>
                        </h5>
                    </div>
                    <div id=\"collapseThree\" class=\"collapse collapseCenter\" aria-labelledby=\"headingThree\"
                         data-parent=\"#accordion\">
                        <ul>";
                    // <li><a href=\"javascript:;\">10.12.2018</a></li>
                    foreach ($fileDates as $row) {
                        $store = "";
                        $a = $row["diag_date"];
                        $diag_id = $row["id"];
                        if (isset($_GET['store'])) {
                            $store = $_GET['store'];
                        }
                        $test = password_hash($dbName, PASSWORD_BCRYPT, ['cost' => 12]);
                        echo " <li><a data-date='$diag_id' data-store='$store' id='$test' href=\"javascript:;\">$a</a></li> ";
                    }


                    echo "</ul>
                    </div>";

                }
            } else {
                echo "getFileAuthority data çekilemedi";
                echo "<br>";
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    } else return;

}

//TODO: dosya görüntüleme yetkilerinin alınması---------------
//TODO: to get accesible file's date list for date and related id called by getFileAuthority
// erisilen dosyaların tarihlerini ve id'lerini cekmek için, getFileAuthority çağırıyor

function getFileDates($dbName, $storeNumber)
{
    try {
        $db = connectDB($dbName);

        /*echo " diag_detail baglanti basarili";
        echo "<br>";*/
        $findDiagDates = $db->prepare("SELECT `diag_date`,`id` FROM `diag_detail`WHERE `store_number`='$storeNumber'");
        $findDiagDates->execute();
        $fDN = $findDiagDates->fetchAll();
        if ($fDN != null) {
            return $fDN;
            /* foreach ($fDN as $row) {
                echo "id: " . $row["id"] . "  date " . $row["diag_date"] . "<br>";
                echo "<br>";
            }*/
        } else {

            echo "getFileDates data çekilemedi";
            echo "<br>";
        }

    } catch (PDOException $e) {
        echo "getFileDates Error: " . $e->getMessage();
        return;

    }


}

?>

<div class="auditsWrapper">
    <div class="auditsContainer">
        <!-- AUDIT PART -->
        <div class="auditsToogleBox">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <span>Audits Seçin</span>
                        <h5 class="mb-0 text-center">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                Audits
                            </button>
                        </h5>
                    </div>


                    <div id="collapseOne" class="collapse show collapseCenter" aria-labelledby="headingOne"
                         data-parent="#accordion">
                        <ul>
                            <li><a href="javascript:;">10.12.2018</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <input class="addAudit" type="button" value="ekle"/>
        </div><!-- auditsToogleBox -->

        <!-- DIAGNOSTIC PART -->
        <div class="auditsToogleBox">
            <div id="accordion">
                <div class="card">
                    <span>Diagnostic Seçin</span>
                    <?php
                    getFileAuthority();
                    ?>
                </div>
            </div>
            <input class="addDiag" type="button" value="ekle"/>
        </div><!-- auditsToogleBox -->


    </div><!-- auditsContainer -->

    <!-- <div class="auditsButton">
    <a href="./safetypage.php" class="cta right yellow add_to_cart_button">sonraki adım</a>
    </div> --><!-- auditsButton -->

</div><!-- auditsWrapper -->

<!-----


<div class="card-header" id="headingThree">
                        <h5 class="mb-0 text-center">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                Safety Diagnostic
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse collapseCenter" aria-labelledby="headingThree"
                         data-parent="#accordion">
                        <ul>
                            <li><a href="javascript:;">10.12.2018</a></li>

                        </ul>
                    </div>

------>